<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\SearchUserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class FriendsController extends AbstractController
{
    /**
     * @Route("/dashboard/friends", name="friends")
     * @param PaginatorInterface $paginator
     * @param UserRepository $userRepository
     * @param Request $request
     * @param SessionInterface $session
     * @return Response
     */
    public function index(PaginatorInterface $paginator, UserRepository $userRepository, Request $request, SessionInterface $session): Response
    {
        $formSearchUser = $this->createForm(SearchUserType::class);
        $formSearchUser->handleRequest($request);

        if ($formSearchUser->isSubmitted() && $formSearchUser->isValid()) {
            $session->set('user_search_filter', $formSearchUser->get('email')->getData());
            return $this->redirectToRoute('list_users_to_add_friends');
        }

        $friends = $userRepository->getFriends($this->getUser());

        $pagination = $paginator->paginate(
            $friends,
            $request->query->getInt('page',1),
            10
        );

        return $this->render('friends/index.html.twig', [
            'friends' => $pagination,
            'searchUser' => $formSearchUser->createView()
        ]);
    }

    /**
     * @param UserRepository $userRepository
     * @param Request $request
     * @Route("/dashboard/user", name="find_user_by_name", methods={"GET"})
     */
    public function getUsers(UserRepository $userRepository, Request $request)
    {
        $users = $userRepository->findAllMatching($request->request->get('query'));
    }

    /**
     * @Route("/dashboard/friends/search", name="list_users_to_add_friends")
     * @param SessionInterface $session
     * @param UserRepository $userRepository
     * @return Response
     */
    public function listUsersToAddFriends(SessionInterface $session, UserRepository $userRepository): Response
    {
        $emailSearch = $session->get('user_search_filter');

        $users = $userRepository->findBy(['email' => $emailSearch]);

        return $this->render('friends/showuser.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/dashboard/send_friend_request/{id}", name="send_friend_request")
     * @param int $id
     * @param UserRepository $userRepository
     * @param EntityManager $entityManager
     * @return RedirectResponse
     * @throws ORMException
     */
    public function sendFriendRequest(int $id, UserRepository $userRepository, EntityManager $entityManager): RedirectResponse
    {
        // TODO: Secure this route with post route and multiple form
        $userToAdd = $userRepository->find($id);
        /** @var User $user */
        $user = $this->getUser();
        if(!in_array($userToAdd,$user->getFriends())) {
            $user->getFriends()->add($userToAdd);
            $entityManager->persist($user);
        }
        return $this->redirectToRoute('friends');
    }
}
